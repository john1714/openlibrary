<?php
namespace App\Support\Repository;

use App\Support\Repository\Interfaces\UserRepositoryInterface;
use App\User;

class UserRepositoryEloquent implements UserRepositoryInterface
{
    private $user;

    public function __construct()
    {
        $this->user = new User();
    }

    public function find(int $id)
    {
        return $this->user->find($id);
    }

    public function findAll()
    {
        return $this->user->all();
    }

    public function create(array $data)
    {
        return $this->user->create($data);
    }

    public function update(array $data, $id)
    {
        return $this->user->find($id)->update($data);
    }

    public function firstOrCreate(array $data)
    {
        return $this->user->firstOrCreate($data);
    }

    public function delete($id)
    {
        return $this->user->find($id)->delete();
    }

}