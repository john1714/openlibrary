@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->

      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-12">
            <book-cards></book-cards>
          <!-- /.card -->
        </div>
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <create-book-modal></create-book-modal>

@endsection
 
@section('javascript')
<!-- jQuery -->

@stop