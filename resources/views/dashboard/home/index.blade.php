@extends('layouts.master')

@section('page_title')
    Home
@endsection

@section('header')
    <div class="col-md-6">
        <h1 class="m-0 text-dark">Seus Livros</h1>
    </div><!-- /.col -->
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->

   <book-cards-rent></book-cards-rent>
   <div style="margin-top: 70px !important;"></div>


    <div class="col-md-12">
        <h3 class="">Suas Reservas</h3>
        <book-cards-reserved></book-cards-reserved>
    </div>



@endsection
 
@section('javascript')
<!-- jQuery -->

@stop