import Vue from 'vue'
import Vuex from 'vuex'

import books from './modules/books';
import users from './modules/users';

Vue.use(Vuex)

export const mutations = {
    showModal(state, componentName) {
        state.modalVisible = true;
        state.modalComponent = componentName;
    },
    hideModal(state) {
        state.modalVisible = false;
    },
};

export const state = {
    modalVisible: false,
    modalComponent: null,
};

export default new Vuex.Store({
    mutations,
    state,
    modules: {
        books,
        users
    }
})