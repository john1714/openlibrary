<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\BusinessException;
use App\Support\Repository\Interfaces\BookRepositoryInterface;
use App\Support\Repository\ReservationRepositoryEloquent;
use App\Support\Services\BooksToApiResponse;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ReservationController extends Controller
{

    private $reservation;

    private $book;

    private $auth;

    private $idUser;

    public function __construct(ReservationRepositoryEloquent $repository, BookRepositoryInterface $bookRepository)
    {
        $this->middleware('auth');
        $this->auth = auth();
        $this->idUser = Auth::id();
        $this->reservation = $repository;
        $this->book = $bookRepository;
    }

    public function index(Request $request)
    {

        $id = $this->auth->id();
        $status = $request->get('status');
        $books = $this->reservation->booksByUser($id, $status);
        return (new BooksToApiResponse($books))->run() ;
    }

    public function myReservations()
    {
        if($this->idUser == null){
            return response()->json([]);
        }
        $reservations = $this->reservation->find($this->idUser);
        return response()->json($reservations);
    }

    public function create()
    {
        //
    }

    public function rentBook(Request $request)
    {
        try {

            $book_id     = $request->post('book_id');
            $user_id     = $request->post('user_id');
            $number_days = $request->post('number_days');

            $book = $this->book->find($book_id);

            if(!$book) {
                throw new BusinessException("Livro inválido");
            }

            if(!is_numeric($number_days)) {
                throw new BusinessException("Quantidade de dia(s) inválido");
            }

            if(!User::find($user_id)){
                throw new BusinessException("Usuário inválido");
            }

            $reservation = new \App\Support\Services\Book\Reservation($book, $user_id);

            $reservation->rent($number_days);

            return response()->json([
                'success' => true,
                'message' => 'Livro alugado com sucesso'
            ], 200);

        }catch (BusinessException $exception){
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage(),
                'instance' => BusinessException::class
            ],400);
        }
    }

    public function reservationBook(Request $request)
    {
        try {

            $book_id = $request->post('book_id');

            if(!is_numeric($book_id)){
                throw new BusinessException("Parametro inválido");
            }

            $book = $this->book->find($book_id);

            if(!$book) {
                throw new BusinessException("Livro inválido");
            }

            $reservation = new \App\Support\Services\Book\Reservation($book, $this->auth->id());

            $reservation->reservation();

            return response()->json([
                'success' => true,
                'message' => 'Livro reservado com sucesso'
            ], 200);

        }catch (BusinessException $exception){
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage(),
                'instance' => BusinessException::class
            ],400);
        }
    }

    public function giveBackBook(Request $request)
    {
        try {

            $book_id = $request->post('book_id');
            $user_id = $request->post('user_id');

            if(!is_numeric($book_id)){
                throw new BusinessException("Parametro inválido");
            }

            if(!is_numeric($user_id)){
                throw new BusinessException("Parametro inválido");
            }

            $book = $this->book->find($book_id);

            if(!$book) {
                throw new BusinessException("Livro inválido");
            }

            $reservation = new \App\Support\Services\Book\Reservation($book, $user_id);

            $reservation->giveback();

            return response()->json([
                'success' => true,
                'message' => 'Livro devolvido com sucesso'
            ], 200);

        }catch (BusinessException $exception){
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage(),
                'instance' => BusinessException::class
            ],400);
        }
    }


    public function desistReservation(Request $request){
        try {

            $book_id = $request->post('book_id');

            if(!is_numeric($book_id)){
                throw new BusinessException("Parametro inválido");
            }

            $book = $this->book->find($book_id);

            if(!$book) {
                throw new BusinessException("Livro inválido");
            }

            $reservation = new \App\Support\Services\Book\Reservation($book, $this->auth->id());

            $reservation->desist();

            return response()->json([
                'success' => true,
                'message' => 'Concluido'
            ], 200);

        }catch (BusinessException $exception){
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage(),
                'instance' => BusinessException::class
            ],400);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
