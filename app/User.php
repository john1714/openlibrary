<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    const PROFILE_ADMIN = 'admin';

    const PROFILE_CLERK = 'clerk';

    const PROFILE_READER = 'reader';

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','type','address','phone','cpf'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdmin()
    {
        return $this->type == self::PROFILE_ADMIN;
    }

    public function isClerk()
    {
        return $this->type == self::PROFILE_CLERK;
    }

    public function isReader()
    {
        return $this->type == self::PROFILE_READER;
    }
}
