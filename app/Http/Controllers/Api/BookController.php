<?php

namespace App\Http\Controllers\Api;

use App\Support\Models\Book;
use App\Support\Repository\Interfaces\BookRepositoryInterface;
use App\Support\Services\BooksToApiResponse;
use App\Support\Services\CreateBook;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use File;

class BookController extends Controller
{

    protected $auth;

    protected $book;

    public function __construct(BookRepositoryInterface $book)
    {
        $this->auth = auth();
        $this->book = $book;
    }

    public function index()
    {
        $all = $this->book->findAll();
        return response()->json((new BooksToApiResponse($all))->run());
    }

    public function store(Request $request)
    {

        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'mimes:jpeg,jpg,png,gif|required'
        ]);

        $image = $request->file('image');
        $filename = $image->getClientOriginalName();
        $filename = base64_encode($filename . '-' . date('H:i:s')) . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('/img/photos/'), $filename);

        $book = new Book();
        $book->user_created_id = $request->user()->id;
        $book->title = $request->input('title');
        $book->description = $request->input('description');
        $book->year = $request->input('year') ?? 0;
        $book->author = $request->input('author') ?? '';
        $book->publishing_company = $request->input('publishing_company');
        $book->image = $filename;

        $book = (new CreateBook($book))->save();

        return response()->json([
            'success' => true,
            'data' => $book
        ], 200);

    }


    public function show($id)
    {
        $book = $this->book->find($id);

        if (!$book) {
            return response()->json([]);
        }

        $book = collect([$book]);
        $book = array_first((new BooksToApiResponse($book))->run());
        return response()->json($book);
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        $this->authorize('delete', Book::class);

        if (!is_numeric($id)) {
            return response()->json([
                'success' => false,
                'message' => 'Não foi possivel excluir o item'
            ], 200);
        }

        $book = $this->book->find($id);

        $image_path = public_path("/img/photos/$book->image");
        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        $this->book->delete($id);

        return response()->json([
            'success' => true,
        ], 200);
    }

    public function reservationBook(Request $request)
    {
        $book_id = $request->post('book_id');
        $book = $this->book->find($book_id);
        $idUser = $this->auth->id();
        $reservation = new \App\Support\Services\Book\Reservation($book, $idUser);
        $reservation->reservation();
        return response()->json([
            'success' => true,
            'data' => ''
        ], 200);
    }

    public function rentBook(Request $request)
    {
        $book_id = $request->post('book_id');
        $user_id = $request->post('user_id');

        $book = $this->book->find($book_id);

        $reservation = new \App\Support\Services\Book\Reservation($book, $user_id);

        $reservation->rent();

        return response()->json([
            'success' => true,
            'data' => $data
        ], 200);
    }
}
