<?php

namespace App\Support\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{

    protected $table = "reservations";

    protected $fillable = [
      'user_id',
      'book_id',
      'delivery_date',
      'status'
    ];

    public function book()
    {
        return $this->hasOne(Book::class,'user_id','id');
    }

    public function user()
    {
        return $this->hasMany(User::class, 'id','user_id');
    }
}
