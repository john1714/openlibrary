<?php

namespace App\Support\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
        'user_created_id'   ,
        'title'             ,
        'description'       ,
        'year'              ,
        'author'            ,
        'image'             ,
        'publishing_company',
    ];

    public function reservation()
    {
        return $this->hasMany(Reservation::class)
            ->orderByDesc('id');
    }
}
