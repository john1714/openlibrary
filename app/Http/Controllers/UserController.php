<?php

namespace App\Http\Controllers;

use App\Exceptions\BusinessException;
use App\Http\Requests\CreateUserRequest;
use App\Support\Models\Type;
use App\Support\Repository\Interfaces\UserRepositoryInterface;
use App\User;
use Illuminate\Http\Request;
use Validation;

class UserController extends Controller
{

    private $types;

    private $user_repository;

    public function __construct(UserRepositoryInterface $repository)
    {
        $this->middleware('auth');
        $this->user_repository = $repository;
        $this->types = (new Type())->list();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('dashboard.user.users', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.user.user-store');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {

        try{

            User::create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => bcrypt($request->input('password')),
                'type'     => $request->input('type'),
                'address'  => $request->input('address'),
                'phone'    => $request->input('phone'),
                'cpf'      => $request->input('cpf'),
            ]);

            return redirect()
                ->route('user-create')
                ->with('message', 'Usuário cadastrado');

        }catch (\Exception $exception){
            return redirect()->route('user-create')
                ->withInput()
                ->with('error', $exception->getMessage());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        return view('dashboard.user.user-edit', [
            'user' => $user,
            'types' => $this->types
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try{

            if(!is_numeric($id)){
                throw new BusinessException("Código inválido");
            }

            User::where('id',$id)
                ->update($request->except(['_token']));

            return redirect()
                ->back()
                ->with('message', 'Dados atualizados');

        }
        catch (BusinessException $businessException){
            return redirect()
                ->back()
                ->withInput()
                ->with('error', $businessException->getMessage());
        }
        catch (\Exception $exception){
            return redirect()
                ->back()
                ->withInput()
                ->with('error', $exception->getMessage());
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $user = User::find($id);

            if($user){
                if($user->type == 'admin'){
                    throw new BusinessException("Não é possivel excluir o administrador");
                }
            }

            $user->delete();

            return redirect()
                ->back()
                ->with('message', 'Usuário excluido');

        } catch (\Exception $exception){

            return redirect()
                ->back()
                ->withInput()
                ->with('error', $exception->getMessage());

        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reset()
    {
        return view('dashboard.user.user-reset-password');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $password
     * @param  string  $password_confirm
     * @return \Illuminate\Http\Response
     */
    public function resetPassword(Request $request)
    {

        $password         = $request->post('password');
        $password_confirm = $request->post('password_confirm');

        $validator = \Validator::make($request->all(), [
            'password' => 'required|min:4',
            'password_confirm' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }


        $this->user_repository->update();


    }
}
