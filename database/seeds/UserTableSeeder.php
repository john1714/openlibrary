<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
            'name' => 'Administrador',
            'email' => 'admin@mail.com',
            'password' => bcrypt('qwe123'),
            'type'     => 'admin'
        ]);
    }
}
