<?php

namespace App\Support\Services\Book;

use App\Support\Repository\ReservationRepositoryEloquent;
use App\Exceptions\BusinessException;
use App\Support\Models\Book;
use DateTime;



class Reservation
{

    protected const STATUS_RENT = 'AL';

    protected const STATUS_RESERVED = 'RS';

    protected const STATUS_GIVEBACK = 'EN';

    private $book;

    private $reservation;

    private $user_id;

    private $status = "RS";

    private $delivery_date;

    public function __construct(Book $book, int $user_id)
    {
        $this->reservation = new ReservationRepositoryEloquent();
        $this->book        = $book;
        $this->user_id     = $user_id;
    }


    private function verifyIsReserved(Book $book)
    {
        $result = $this->reservation->findLastBookResevation($book->id);
        if($result){
            return $result->status == self::STATUS_RESERVED;
        }
        return false;
    }

    private function verifyIsGiveback(Book $book)
    {
        $result = $this->reservation->findLastBookResevation($book->id);
        if($result){
            return $result->status == self::STATUS_GIVEBACK;
        }
        return false;
    }

    public function rent($number_days)
    {

        $d = "+$number_days day";

        $this->delivery_date = (new \DateTime($d))->format('Y-m-d 00:00:00');

        $book = $this->reservation->findLastBookResevation($this->book->id);

        if($book){
            if($book->status == self::STATUS_RENT){
                Throw new BusinessException("Este livro já se encontra alugado.");
            }
        }

        $this->status = self::STATUS_RENT;

        return $this->run();

    }

    public function reservation()
    {
        $this->delivery_date = (new DateTime('+1 day'))->format('Y-m-d H:i:s');

        if($this->verifyIsReserved($this->book)){
            Throw new BusinessException("Este livro já se encontra reservado.");
        }
        $this->status = self::STATUS_RESERVED;
        return $this->run();
    }

    public function giveback()
    {
        $this->delivery_date = date('Y-m-d H:i:s');

        if($this->verifyIsGiveback($this->book)){
            Throw new BusinessException("Este livro já foi devolvido.");
        }
        $this->status = self::STATUS_GIVEBACK;

        return $this->run();
    }

    public function desist()
    {
        if(!$this->verifyIsReserved($this->book)){
            Throw new BusinessException("Para deistir, este livro precisa está reservado, em caso de problema técnico, contate a atendente para que enre com contato com o suporte.");
        }
        $this->status = self::STATUS_GIVEBACK;
        return $this->run();
    }

    private function run()
    {
        $this->reservation->create([
            'user_id' => $this->user_id,
            'book_id' => $this->book->id,
            'status'  => $this->status,
            'delivery_date' => $this->delivery_date
        ]);
    }
}