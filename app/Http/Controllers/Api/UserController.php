<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    private $user;

    public function __construct()
    {
        $this->user = (new User());
    }

    public function users()
    {
        return $this->user->all();
    }

}
