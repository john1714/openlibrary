export default {

    state: {
        items: [],
        items_reserved: [],
        items_rent: [],
        form: {
            title       : '',
            description : '',
            year        : '',
            author      : '',
            image       : null,
            publishing_company : '',
        },
        message: '',
        error: ''
    },

    mutations: {

        LOAD_BOOKS (state, books){
            state.items = books.slice(0).reverse();
        },

        LOAD_BOOKS_RESERVEDS (state, books){
            state.items_reserved = books.slice(0).reverse();
        },

        LOAD_BOOKS_RENT (state, books){
            state.items_rent = books.slice(0).reverse();
        },

        LOAD_RESERVATION_BOOKS (state, books){
            state.items = books.slice(0).reverse();
        },

        ADD_BOOK (state, book) {
            state.items.unshift(book)
        },

        SHOW_MESSAGE (state, message) {
            state.message = message
        },

        SHOW_ERROR (state, message) {
            state.error = message
        },

        RESET_FORM (state) {
            state.form = {}
        },

        DELETE_BOOK (state, index) {
            console.log(index);
            state.items.splice(index,1)
        },

        RESERVATION_BOOK (state, id) {

        }
    },

    actions: {
        loadBooks (context) {
            var vm = this;
            axios.get('/api/books')
                    .then(response => {
                        context.commit('LOAD_BOOKS', response.data)
                    }, err => {
                       this.$toast('teste');
                    });
        },

        loadBooksReservateds (context) {
            axios.get('/api/reservations?status=RS')
                .then(response => {
                    context.commit('LOAD_BOOKS_RESERVEDS', response.data)
                })
        },

        loadBooksRent (context) {
            axios.get('/api/reservations?status=AL')
                .then(response => {
                    context.commit('LOAD_BOOKS_RENT', response.data)
                })
        },

        showError(context, msg) {
            context.commit('SHOW_ERROR', msg);
        },

        findBook (context, param) {
            return axios.get('/api/books/find/' + param.id);
        },

        loadReservationBooks (context) {
            axios.get('/api/book/reservation')
                .then(response => {
                    context.commit('LOAD_RESERVATION_BOOKS', response.data)
                })
        },

        storeBook (context, params) {
            return axios.post('/api/book', params)
        },
        rentBook (context, param) {
            return axios.post('/api/book/rent', {book_id: param.book_id, user_id: param.user_id, number_days: param.number_days });
        },
        reservedBook (context, param) {
            return axios.post('/api/book/reserved', {book_id: param.id });
        },
        giveBack (context, param) {
            return axios.post('/api/book/giveback', {book_id: param.id, user_id: param.user_id});
        },
        desistReservation (context, param) {
            return axios.post('/api/book/desistreservation', {book_id: param.id});
        },
        deleteBook (context, param) {
            return axios.post('/api/book/delete/' + param.id);
        }


    }
}