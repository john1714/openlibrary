<?php

namespace App\Support\Repository;

use App\Support\Models\Book;
use App\Support\Models\Reservation;

class ReservationRepositoryEloquent
{

    private $model;

    private $book;

    public function __construct()
    {
        $this->model = new Reservation();

        $this->book = new Book();
    }

    public function find(int $id)
    {
        return $this->model->with(['book','user'])->find($id);
    }

    public function findLastBookResevation(int $id, $status = "")
    {
        $query = $this->model->newQuery();
        if($status){
            $query->where('status', $status);
        }
        $query->where(['book_id' => $id]);
        return $query->orderBy('id', 'desc')->first();

    }

    public function booksByUser(int $userId, $status)
    {
        $result = \DB::select("SELECT r1.book_id
            FROM reservations r1
                   INNER JOIN (SELECT r2.book_id, MAX(r2.id) as maxid
                               FROM reservations r2
                               GROUP BY r2.book_id
                              ) r3 ON r3.maxid = r1.id
                    inner join books b on r1.book_id = b.id
            where r1.status = ? and r1.user_id = ?", [$status,$userId]);

        if(empty($result)) return [];

        $ids = [];
        foreach ($result as $item){
            $ids[] = $item->book_id;
        }
        $query = $this->book->newQuery();
        $query->whereIn('id', $ids);
        return $query->get()->load(['reservation','reservation.user']);

    }

    public function findAll()
    {
        return $this->model->with('user','book')->all();
    }

    public function where(array $param)
    {
        return $this->model->where($param)->get();
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function delete($id)
    {
        return $this->model->find($id)->delete();
    }

    public function firstOrCreate(array $data)
    {
        return $this->model->firstOrCreate($data);
    }

    public function update(array $data, $id)
    {
        return $this->model->find($id)->update($data);
    }

    private function load($query)
    {
        return $query;
    }

}