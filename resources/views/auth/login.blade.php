@extends('layouts.master')

@section('style_css')
    <style>
        body {
            background-color: #f7f7f7;
        }

        .form-control {
            border-radius: 0 !important;
        }

        .callout, .card, .info-box, .mb-3, .my-3, .small-box {
            margin-bottom: 2.2rem!important;
        }

        @media (min-width: 768px){
            .offset-md-3 {
                margin-left: 28% !important;
            }
        }
    </style>
@endsection

@section('content')

    <div id="box-login">
        <div class="row">
            <div class="col-md-5 offset-md-3">
                <div class="card mb-8 flex-row" style="padding: 30px; box-shadow: 1px 1px 2px 0.1px #1b4b72;">
                    <div class="col-md-4">
                     <img src="{{ asset('img/logo.jpg') }}" style="width: 160px; margin-top: 0px; height: 140px; top:40px;" alt="Card image"
                         class="card-img-lef col-md-12">
                    </div>
                    <div class="card-body col-md-12">

                        <form action="{{ route('login') }}" method="post">
                            @csrf
                            <div class="input-group mb-3">
                                <input id="email" type="email"
                                       class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                                       value="{{ old('email') }}" placeholder="Email" required autofocus>
                            </div>
                            <div class="input-group mb-3">
                                <input id="password" type="password"
                                       class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                       name="password"
                                       placeholder="Senha"
                                       required>
                                <div class="input-group-append">
                                    <span class="fa fa-lock input-group-text"></span> @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span> @endif
                                </div>
                            </div>
                            <div class="row">
                                <!-- /.col -->
                                <div class="col-4">
                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
                                </div>
                                @if ($errors->has('email'))
                                    <div class="col-8" style="line-height: 37px !important;">
                                        <strong style="color: red">{{ $errors->first('email') }}</strong>
                                    </div>
                                @endif

                                <!-- /.col -->
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- /.login-box -->
@endsection