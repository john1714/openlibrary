<?php

namespace App\Support\Services;


use App\Exceptions\UserNotFoundException;
use App\Support\Models\Book;
use App\Support\Repository\BookRepositoryEloquent;
use App\User;
use Illuminate\Support\Facades\Auth;

class CreateBook
{

    protected $model;

    protected $user;

    protected $repository;

    public function __construct(Book $book)
    {
        $this->model = $book;
        $this->repository = new BookRepositoryEloquent();
    }

    public function save()
    {
        $book = $this->model;
        $book->user_id = ($book->user_id) ? $book->user_id : $this->getUserIdLoggged();
        return $this->repository->create($book->toArray());
    }

    private function getUserIdLoggged()
    {
        $id = null;

        if($this->model->user_id == null) {
            if($id = Auth::id()){
                $id = $id;
            }
        }else{
            $id = $this->model->user_id;
        }

        if(!$id){
            Throw new UserNotFoundException("User not found error");
        }

        return $id;
    }

}