@extends('layouts.master')

@section('page_title')
    Cadastro de usuário
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->

    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-12">

            @if(session('message'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h6 style="margin-bottom: 0px;"><i class="icon fa fa-check"></i>{{ session('message') }}</h6>
                </div>
            @endif

            @if(session('error'))
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h6 style="margin-bottom: 0px;"><i class="icon fa fa-ban"></i>{{ session('error') }}</h6>
                </div>
            @endif

            <div class="card card-primary">
                <!-- /.card-header -->
                <!-- form start -->
                <form method="post" action="{{ route('user-store') }}" role="form">
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="nome">Nome: <span class="text-danger">@error('name') {{ $message }}
                                        @enderror</span></label>
                                <input type="text" value="{{ old('name') }}"
                                       class="form-control @error('name') is-invalid @enderror" name="name" id="nome"
                                       placeholder="">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="exampleInputEmail1">Email: <span
                                            class="text-danger">@error('email') {{ $message }} @enderror</span></label>
                                <input type="text" value="{{ old('email') }}"
                                       class="form-control @error('email') is-invalid @enderror" name="email" id="email"
                                       placeholder="">
                            </div>

                            <div class="form-group col-md-4">
                                <label for="endereco">Endereço: <span
                                            class="text-danger">@error('address') {{ $message }}
                                        @enderror</span></label>
                                <input type="text" value="{{ old('address') }}"
                                       class="form-control @error('address') is-invalid @enderror" name="address"
                                       id="endereco" placeholder="">
                            </div>

                            <div class="form-group col-md-4">
                                <label for="telefone">Telefone: <span class="text-danger">@error('phone') {{ $message }}
                                        @enderror</span></label>
                                <input type="text" value="{{ old('phone') }}"
                                       class="form-control @error('phone') is-invalid @enderror" name="phone"
                                       id="telefone" placeholder="">
                            </div>

                            <div class="form-group col-md-4">
                                <label for="cpf">CPF: <span class="text-danger">@error('cpf') {{ $message }}
                                        @enderror</span></label>
                                <input type="text" value="{{ old('cpf') }}"
                                       class="form-control @error('cpf') is-invalid @enderror" name="cpf" id="cpf"
                                       placeholder="">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="senha">Senha: <span class="text-danger">@error('password') {{ $message }}
                                        @enderror</span></label>
                                <input type="password" value="{{ old('password') }}"
                                       class="form-control @error('password') is-invalid @enderror" name="password"
                                       id="senha" placeholder="">
                            </div>
                            <br/>
                            <div class="form-group col-md-6">
                                <label for="tipo">Tipo: <span class="text-danger">@error('type') {{ $message }}
                                        @enderror</span></label>
                                <select class="form-control @error('type') is-invalid @enderror" name="type">
                                    <option value="">Selecione</option>
                                    @foreach( (new \App\Support\Models\Type)->list() as $item)
                                        <option value="{{ $item['type'] }}">{{ $item['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    @csrf
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Cadastrar</button>
                    </div>
                </form>
            </div>

        </div>

    </div>
    <!-- /.row -->
    <!-- Main row -->
@endsection

@section('javascript')
    <!-- jQuery -->

@stop