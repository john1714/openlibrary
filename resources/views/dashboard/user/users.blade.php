@extends('layouts.master')

@section('page_title')
    Usuários
@endsection

@section('header')
    <style>
        #tbody-user tr td span {
                line-height: 35px !important;
        }
    </style>
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->

      <!-- Small boxes (Stat box) -->
      <div class="row">

          <div class="col-md-12">

              <div class="card">
                  <div class="card-header">
                      <h3 class="card-title">Registros</h3>

                      <div class="card-tools">
                          <div class="input-group input-group-sm" style="width: 150px;">
                              <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                              <div class="input-group-append">
                                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                              </div>
                          </div>
                      </div>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body table-responsive p-0">
                      <table class="table table-hover">
                          <tbody id="tbody-user">
                          <tr>
                              <th>Id</th>
                              <th>Nome</th>
                              <th>Email</th>
                              <th>Endereço</th>
                              <th>Telefone</th>
                              <th>CPF</th>
                              <th>Perfil</th>
                              <th>-</th>
                          </tr>
                          @foreach($users as $user)
                          <tr>
                              <td><span>{{ $user->id }}</span></td>
                              <td><span>{{ $user->name }}</span></td>
                              <td><span>{{ $user->email }}</span></td>
                              <td><span>{{ $user->address }}</span></td>
                              <td><span>{{ $user->phone }}</span></td>
                              <td><span>{{ $user->type }}</span></td>
                              @if($user->type == 'admin')
                                  <td><span>Administrador</span></td>
                              @elseif($user->type == 'clerk')
                                  <td><span>Atendente</span></td>
                              @elseif($user->type == 'reader')
                                  <td><span>Leitor</span></td>
                              @endif
                              <td><a href="{{ route('user-edit',[$user->id]) }}" type="button" class="btn btn-sm btn-warning" style="color: #FFFFFF;"><i class="fa fa-pencil"></i></a></td>
                          </tr>
                              @endforeach
                          </tbody></table>
                  </div>
                  <!-- /.card-body -->
              </div>

          </div>

      </div>
      <!-- /.row -->
      <!-- Main row -->
@endsection
 
@section('javascript')
<!-- jQuery -->

@stop