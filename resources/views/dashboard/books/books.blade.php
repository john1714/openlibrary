@extends('layouts.master')

@section('page_title')
    Livros
@endsection

@section('header')

    <div class="row mb-2">
        <div class="col-sm-6">
        </div>
        <!-- /.col -->
        @if(auth()->user()->type == 'admin')
        <div class="col-sm-6">
            <h1 class="m-0 text-dark float-sm-right">
                <button class="btn btn-sm btn-primary" v-b-modal.modal-1 size="sm"><i class="fa fa-plus"></i> Add Item</button></h1>
        </div>
        @endif
        <!-- /.col -->
    </div>

@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->

      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-12">
            <books></books>
          <!-- /.card -->
        </div>
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <create-book-modal></create-book-modal>

@endsection
 
@section('javascript')
<!-- jQuery -->

@stop