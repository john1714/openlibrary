<?php

namespace App\Support\Models;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{

    private $types = [
        [
            'type' => 'admin',
            'name' => 'Administrador'
        ],
        [
            'type' => 'clerk',
            'name' => 'Atendente'
        ],
        [
            'type' => 'reader',
            'name' => 'Leitor'
        ],
    ];

    public function list()
    {
        return $this->types;
    }
}
