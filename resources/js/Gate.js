export default class Gate {

    constructor(user){
        this.user = user;
    }

    isAdmin(){
        return this.user.type == 'admin';
    }

    isClerk(){
        return this.user.type == 'clerk';
    }

    isReader(){
        return this.user.type == 'reader';
    }

    viewDetailReservation(book){

        if(this.isClerk() && (book.is_reservation == true || book.is_rent == true)){
            return true;
        }
        return ( ( book.is_reservation == true || book.is_rent == true)
                && (this.isReader() || this.isClerk()) )
                && (this.user.id == book.reservation.user.id);
    }
}