
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Gate from "./Gate";

Vue.prototype.$gate = new Gate(window.user);
Vue.prototype.$path = { img: window.Laravel.path + 'img/photos/' };

Vue.prototype.$toast = function (message = '') {

    this.$modal.hide('dialog');

    this.$modal.show('dialog', {
        title: message,
        buttons: [{title: 'Close'}]
    })
};

import store from './vuex/store';

import BootstrapVue from 'bootstrap-vue'
import VModal from 'vue-js-modal'

//
// import 'bootstrap/dist/css/bootstrap.css'
// import 'bootstrap-vue/dist/bootstrap-vue.css'

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('books', require('./components/books/Books.vue').default);
Vue.component('book', require('./components/books/Book.vue').default);
Vue.component('book-cards', require('./components/books/BookCards.vue').default);
Vue.component('book-cards-reserved', require('./components/books/BookCardsReserved.vue').default);
Vue.component('book-cards-rent', require('./components/books/BookCardsRent.vue').default);
Vue.component('book-cards', require('./components/books/BookCards.vue').default);
Vue.component('book-cards-reservation', require('./components/books/BookReservationCards.vue').default);
Vue.component('book-card', require('./components/books/BookCard.vue').default);
Vue.component('form-rent-book', require('./components/forms/FormRentBook').default);
Vue.component('create-book-modal', require('./components/books/CreateModalBook.vue').default);
Vue.component('rent-modal-detail', require('./components/books/RentModalDetail.vue').default);
Vue.component('book-modal-detail', require('./components/books/BookModalDetail.vue').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.use(BootstrapVue);
//Vue.use(VModal)

Vue.use(VModal, { dialog: true, dynamic: true, injectModalsContainer: true,
    dynamicDefaults: {
        foo: 'foo'
    }});

const app = new Vue({
    el: '#app',
    store
});
