@extends('layouts.master')

@section('page_title')
    Cadastro de usuário
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->

    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-6 offset-md-3">

            @if(session('message'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h6 style="margin-bottom: 0px;"><i class="icon fa fa-check"></i>{{ session('message') }}</h6>
                </div>
            @endif

            @if(session('error'))
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h6 style="margin-bottom: 0px;"><i class="icon fa fa-ban"></i>{{ session('error') }}</h6>
                </div>
            @endif

            <div class="card card-primary">
                <!-- /.card-header -->
                <!-- form start -->
                <form method="post" action="{{ route('user-reset-password') }}" role="form">
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="password">Senha: <span class="text-danger">@error('password') {{ $message }}
                                        @enderror</span></label>
                                <input type="password" value="{{ old('password') }}"
                                       class="form-control @error('password') is-invalid @enderror" name="password" id="password"
                                       placeholder="">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="password_confirm">Confirmar senha: <span
                                            class="text-danger">@error('password_confirm') {{ $message }} @enderror</span></label>
                                <input type="password" value="{{ old('password_confirm') }}"
                                       class="form-control @error('password_confirm') is-invalid @enderror" name="password_confirm" id="password_confirm"
                                       placeholder="">
                            </div>

                        </div>
                    </div>
                    <!-- /.card-body -->
                    @csrf
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Confirmar</button>
                    </div>
                </form>
            </div>

        </div>

    </div>
    <!-- /.row -->
    <!-- Main row -->
@endsection

@section('javascript')
    <!-- jQuery -->

@stop