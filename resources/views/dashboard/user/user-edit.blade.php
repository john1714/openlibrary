@extends('layouts.master')

@section('page_title')
    Editar usuário
@endsection

@section('header')

    <div class="row mb-2">

    </div>

@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->

    <!-- Small boxes (Stat box) -->
    <div class="row">

        <div class="col-md-12">

            @if(session('message'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h6 style="margin-bottom: 0px;"><i class="icon fa fa-check"></i>{{ session('message') }}</h6>
                </div>
            @endif

            @if(session('error'))
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h6 style="margin-bottom: 0px;"><i class="icon fa fa-ban"></i>{{ session('error') }}</h6>
                </div>
            @endif

            <div class="card card-primary">
                <!-- /.card-header -->
                <!-- form start -->
                <form method="post" action="{{ route('user-update',[$user->id]) }}" role="form">
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="nome">Nome: <span class="text-danger">@error('name') {{ $message }}
                                        @enderror</span></label>
                                <input type="text" value="{{ $user->name }}"
                                       class="form-control @error('name') is-invalid @enderror" name="name" id="nome"
                                       placeholder="">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="exampleInputEmail1">Email: <span
                                            class="text-danger">@error('email') {{ $message }} @enderror</span></label>
                                <input type="email" value="{{ $user->email }}"
                                       class="form-control @error('email') is-invalid @enderror" name="email" id="email"
                                       placeholder="">
                            </div>

                            <div class="form-group col-md-4">
                                <label for="endereco">Endereço: <span
                                            class="text-danger">@error('address') {{ $message }}
                                        @enderror</span></label>
                                <input type="text" value="{{ $user->address }}"
                                       class="form-control @error('address') is-invalid @enderror" name="address"
                                       id="endereco" placeholder="">
                            </div>

                            <div class="form-group col-md-4">
                                <label for="telefone">Telefone: <span class="text-danger">@error('phone') {{ $message }}
                                        @enderror</span></label>
                                <input type="text" value="{{ $user->phone }}"
                                       class="form-control @error('phone') is-invalid @enderror" name="phone"
                                       id="telefone" placeholder="">
                            </div>

                            <div class="form-group col-md-4">
                                <label for="cpf">CPF: <span class="text-danger">@error('cpf') {{ $message }}
                                        @enderror</span></label>
                                <input type="text" value="{{ $user->cpf }}"
                                       class="form-control @error('cpf') is-invalid @enderror" name="cpf" id="cpf"
                                       placeholder="">
                            </div>
                            @csrf
                            <br/>
                            <div class="form-group col-md-6">
                                <label for="tipo">Tipo: <span class="text-danger">@error('type') {{ $message }}
                                        @enderror</span></label>
                                <select class="form-control @error('type') is-invalid @enderror" name="type">
                                    @foreach($types as $type)
                                        <option value="{{ $type['type'] }}"
                                        {{ ($user->type == $type['type']) ? 'selected' : '' }}
                                        >{{ $type['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Salvar</button>
                        <button type="button" class="btn btn-danger" onclick="document.getElementById('delete-user-form').submit();">Excluir</button>
                        <a href="{{ route('user-reset-password') }}" class="btn btn-warning" >Alterar senha</a>
                    </div>
                </form>
                <form method="post" id="delete-user-form" action="{{ route('user-delete',[$user->id]) }}"  style="display: none;">
                    @csrf
                </form>
            </div>

        </div>

    </div>
    <!-- /.row -->
    <!-- Main row -->
@endsection

@section('javascript')
    <!-- jQuery -->

@stop