<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'     => 'required',
            'email'    => 'required|email',
            'password' => 'required',
            'type'     => 'required',
            'address'  => 'sometimes|nullable',
            'phone'    => 'sometimes|nullable|celular_com_ddd',
            'cpf'      => 'sometimes|nullable|formato_cpf',
        ];
    }

    public function messages()
    {
        return [
            'celular_com_ddd' => 'Não é válido',
            'required' => 'Campo obrigatório',
            'formato_cpf' => 'formato inválido'
        ];
    }
}
