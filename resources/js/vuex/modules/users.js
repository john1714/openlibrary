export default {

    state: {
        items: [],
    },

    mutations: {

        LOAD_USERS (state, users){
            state.items = users.slice(0).reverse();
        }
    },

    actions: {
        loadUsers (context) {
            axios.get('/api/user/users')
                    .then(response => {
                        context.commit('LOAD_USERS', response.data)
                    })
        }
    }
}