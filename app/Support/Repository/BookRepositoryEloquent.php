<?php

namespace App\Support\Repository;

use App\Support\Models\Book;
use App\Support\Repository\Interfaces\BookRepositoryInterface;

class BookRepositoryEloquent implements BookRepositoryInterface
{

    private $model;

    public function __construct()
    {
        $this->model = new Book();
    }

    public function find(int $id)
    {
        return $this->model->with(['reservation','reservation.user'])->find($id);
    }

    public function findAll()
    {
        return $this->model->all()->load(['reservation','reservation.user']);
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function delete($id)
    {
        return $this->model->find($id)->delete();
    }

    public function firstOrCreate(array $data)
    {
        return $this->model->firstOrCreate($data);
    }

    public function update(array $data, $id)
    {
        return $this->model->find($id)->update($data);
    }

}