<?php

namespace App\Support\Services;


use Illuminate\Database\Eloquent\Collection;

class BooksToApiResponse
{

    private $collection;

    public function __construct($collection)
    {
        $this->collection = $collection;
    }

    public function run()
    {

        $arrayMaster = array();

        foreach ($this->collection as $item) {

            $item = $item->toArray();

            $arrayFormat = array();

            $arrayFormat = [
                'id' => $item['id'],
                'title' => $item['title'],
                'description' => $item['description'],
                'year' => $item['year'],
                'author' => $item['author'],
                'publishing_company' => $item['publishing_company'],
                'stock_quantity' => $item['stock_quantity'],
                'image' => $item['image'],
                'user_id' => $item['user_created_id'],
                'created_at' => $item['created_at'],
                'updated_at' => $item['updated_at'],
                'is_reservation' => false,
                'is_rent' => false,
                "description_status" => "Livre",
                'reservation' => []
            ];


            if (count($item['reservation'])) {


                $reservation = array_first($item['reservation']);

                if ($reservation['status'] == 'RS') {
                    $arrayFormat['is_reservation'] = true;
                    $arrayFormat['description_status'] = 'Reservado';
                }

                if ($reservation['status'] == 'AL') {
                    $arrayFormat['is_rent'] = true;
                    $arrayFormat['description_status'] = 'Alugado';
                }

                if ($reservation['status'] == 'EN') {
                    $arrayFormat['is_reservation'] = false;
                    $arrayFormat['description_status'] = 'Livre';
                }

                $arrayFormat['reservation'] = array_first($item['reservation']);
                $arrayFormat['reservation']['user'] = array_first($item['reservation'][0]['user']);
            }

            $arrayMaster[] = $arrayFormat;

        }

        return $arrayMaster;
    }

}