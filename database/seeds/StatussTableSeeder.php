<?php

use Illuminate\Database\Seeder;

class StatussTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new \App\Support\Models\Status();
        $role->create([
            'prefix' => 'LV',
            'description' => 'Livre'
        ]);

        $role = new \App\Support\Models\Status();
        $role->create([
            'prefix' => 'RS',
            'description' => 'Reservado'
        ]);

        $role = new \App\Support\Models\Status();
        $role->create([
            'prefix' => 'AL',
            'description' => 'Alugado'
        ]);

        $role = new \App\Support\Models\Status();
        $role->create([
            'prefix' => 'EX',
            'description' => 'Expirado'
        ]);
    }
}
