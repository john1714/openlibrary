<?php

namespace App\Support\Services;


use App\Support\Models\Reservation;
use App\Support\Repository\ReservationRepositoryEloquent;

class ReservationBook
{

    protected $reservation;

    protected $repository;

    public function __construct(Reservation $reservation)
    {
        $this->reservation = $reservation;
        $this->repository = new ReservationRepositoryEloquent();
    }

    public function run()
    {
        $this->reservation->delivery_date = date('Y-m-d H:m:i');
        return $this->repository->create($this->reservation->toArray());
    }

}