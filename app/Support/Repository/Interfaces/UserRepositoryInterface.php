<?php

namespace App\Support\Repository\Interfaces;

interface UserRepositoryInterface {

    public function __construct();

    public function find(int $id);

    public function findAll();

    public function create(array $data);

    public function update(array $data, $id);

    public function firstOrCreate(array $data);

    public function delete($id);

}