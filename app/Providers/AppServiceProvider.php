<?php

namespace App\Providers;

use App\Support\Models\Book;
use App\Support\Repository\BookRepositoryEloquent;
use App\Support\Repository\Interfaces\BookRepositoryInterface;
use App\Support\Repository\Interfaces\UserRepositoryInterface;
use App\Support\Repository\UserRepositoryEloquent;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BookRepositoryInterface::class, function(){
            return new BookRepositoryEloquent(new Book());
        });

        $this->app->bind(UserRepositoryInterface::class, function(){
            return new UserRepositoryEloquent();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //\DB::statement('SET FOREIGN_KEY_CHECKS=0');
//               $this->app->bind(BookRepositoryInterface::class,
//                         BookRepositoryEloquent::class);
    }
}

