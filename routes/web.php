<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('/dashboard/home', 'HomeController@index')->name('home');
Route::get('/dashboard/config/create-student', 'UserController@create')->name('user-create');
Route::get('/dashboard/config/users', 'UserController@index')->name('users');
Route::get('/dashboard/config/users/edit/{id}', 'UserController@edit')->name('user-edit');
Route::post('/dashboard/config/users/edit/{id}', 'UserController@update')->name('user-update');
Route::post('/dashboard/config/users/delete/{id}', 'UserController@destroy')->name('user-delete');
Route::get('/dashboard/config/users/reset-password', 'UserController@reset')->name('user-reset-password');
Route::post('/dashboard/config/users/reset-password', 'UserController@resetPassword')->name('user-reset-password');
Route::post('/dashboard/config/cadastro-aluno', 'UserController@store')->name('user-store');
Route::get('/dashboard/books', 'BookController@index')->name('books');
//Route::get('/dashboard/books', 'DashboardController@showBooksCards')->name('books');
Route::get('/dashboard/v2', 'DashboardController@versiontwo')->name('v2');
Route::get('/dashboard/v3', 'DashboardController@versionthree')->name('v3');
Route::get('/dashboard/reservation', 'DashboardController@reservations')->name('book.reservations');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');


Route::group(['prefix' => 'api'], function (){

    Route::get('/books', 'Api\\BookController@index')->name('book.list');
    Route::get('/books/find/{id}', 'Api\\BookController@show')->name('book.show');
    Route::get('/books/find/{id}', 'Api\\BookController@show')->name('book.show');
    Route::post('/book', 'Api\\BookController@store')->name('book.create');
    Route::post('/book/delete/{id}', 'Api\\BookController@destroy')->name('book.delete');
    Route::post('/book/reserved', 'Api\\ReservationController@reservationBook')->name('book.reservation');
    Route::post('/book/rent', 'Api\\ReservationController@rentBook')->name('reservation.rent');
    Route::post('/book/giveback', 'Api\\ReservationController@giveBackBook')->name('reservation.giveback');
    Route::post('/book/desistreservation', 'Api\\ReservationController@desistReservation')->name('reservation.giveback');

    Route::group(['prefix' => 'reservations'], function (){
        Route::get('/', 'Api\\ReservationController@index')->name('reservation.list.user');
    });


    Route::group(['prefix' => 'user'], function (){
        Route::get('/users', 'Api\\UserController@users')->name('user.list');
    });

});