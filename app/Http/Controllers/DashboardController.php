<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function versionone()
    {
        return view('dashboard.v1');
    }
    public function showBooksCards()
    {
        return view('dashboard.books.book-card');
    }

    public function reservations()
    {
        return view('dashboard.books.book-card-reservations');
    }

    public function versionthree()
    {
        return view('dashboard.v3');
    }
}
