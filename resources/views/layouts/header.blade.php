 <!-- Navbar -->
 <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i>
                <span style="margin-left: 10px;">@yield('page_title')</span>
            </a>
        </li>

    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">

    </form>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <!-- Messages Dropdown Menu -->

        <!-- Notifications Dropdown Menu -->

    </ul>
</nav>
<!-- /.navbar -->